﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {
	float sign;
	// Use this for initialization
	void Start () {
		sign = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector3.Distance (Camera.main.transform.position, transform.position);
		if (Input.GetButtonDown ("Fire1") && sign >= 0f) {
			sign = -1f;
		}
		if (distance >= 50f && sign == -1f) {
			sign = 1f;
		} else if (distance <= 2f && sign == 1f) {
			sign = 0f;
		}
		transform.position = Vector3.MoveTowards (transform.position, Camera.main.transform.position, sign * 0.1f);
	}
}
