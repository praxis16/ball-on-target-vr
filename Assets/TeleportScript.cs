﻿using UnityEngine;
using System.Collections;

public class TeleportScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	//as ball collides with target
	void OnTriggerEnter(Collider other) {
		TeleportRandomly ();
		ScoreScript.UpdateScore (1);
	}

	public void TeleportRandomly() {
		Vector3 direction = Random.insideUnitSphere * 20f;
		direction.y = Mathf.Clamp(direction.y, 4f, 20f);
		transform.localPosition = direction;
		transform.LookAt (Camera.main.transform.position);
		transform.Rotate (new Vector3 (1.0f, 0f, 0f), 90);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//called after normal update processes are done
	void LateUpdate() {
		GvrViewer.Instance.UpdateState();
		if (GvrViewer.Instance.BackButtonPressed) {
			Application.Quit();
		}
	}
}
