﻿using UnityEngine;
using System.Collections;

public class ScoreScript : MonoBehaviour {
	static TextMesh Score;
	static int currentScore = 0;
	// Use this for initialization
	void Start () {
		Score = GameObject.FindGameObjectWithTag ("ScoreText").GetComponent<TextMesh> ();
		UpdateScore (currentScore);
	}

	public static void UpdateScore(int incremental) {
		currentScore += incremental;
		Score.text = "Score\n" + currentScore;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
